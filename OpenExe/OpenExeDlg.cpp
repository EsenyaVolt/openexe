
// OpenExeDlg.cpp : ���� ����������
//

#include "stdafx.h"
#include "OpenExe.h"
#include "OpenExeDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// ���������� ���� COpenExeDlg



COpenExeDlg::COpenExeDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(COpenExeDlg::IDD, pParent)
	, way(_T("C:\\Windows\\System32\\*.exe"))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void COpenExeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_WAY, way);
	DDX_Control(pDX, IDC_LIST_EXE, exe_open);
	DDX_Control(pDX, IDC_LIST_PROCESS, exe_close);
}

BEGIN_MESSAGE_MAP(COpenExeDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_EN_CHANGE(IDC_WAY, &COpenExeDlg::OnChangeWay)
	ON_BN_CLICKED(IDC_SEARCH, &COpenExeDlg::OnBnClickedSearch)
	ON_BN_CLICKED(IDC_START, &COpenExeDlg::OnBnClickedStart)
	ON_BN_CLICKED(IDC_TERMINATE, &COpenExeDlg::OnBnClickedTerminate)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// ����������� ��������� COpenExeDlg

BOOL COpenExeDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ������ ������ ��� ����� ����������� ����.  ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������

	// TODO: �������� �������������� �������������
	SetTimer(0, 500, NULL);

	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������.  ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ��������.

void COpenExeDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR COpenExeDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void COpenExeDlg::OnChangeWay()
{
	// TODO:  ���� ��� ������� ���������� RICHEDIT, �� ������� ���������� �� �����
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  �������� ��� �������� ����������
}


void COpenExeDlg::OnBnClickedSearch()
{
	CFileFind file;
	BOOL search = file.FindFile(way);	//����� ������ �� ������

	exe_open.ResetContent();

	while(search)
	{
		search = file.FindNextFile();			//����� ����-�� �����
		CString strname = file.GetFileName();	//�������� ��� �����
		exe_open.AddString(strname);			//���������� ��������� ����� � ��
	}
	
}


void COpenExeDlg::OnBnClickedStart()
{
	// TODO: �������� ���� ��� ����������� �����������
	STARTUPINFO si = { sizeof(si) };
	PROCESS_INFORMATION pi;
	BOOL open_success;

	int i_selected_item = exe_open.GetCurSel();	// ���������� ������ ���������� ��������
	CString ItemSelected;
	exe_open.GetText(i_selected_item, ItemSelected);	//��������� ��������� ������ �� ������ ������
	
	CString  wayL = way;
	wayL.Delete(wayL.GetLength()-5, wayL.GetLength());
	
	CString Path = wayL + ItemSelected;

	open_success = CreateProcess(NULL,
										Path.GetBuffer(),	//�������� ���� � ���������� ��������
										NULL,
										NULL,
										FALSE,
										0,
										NULL,
										NULL,
										&si,
										&pi);
	if (!open_success)
	{
		MessageBox(L"������", L"�� ������� ������� �������", MB_OK);
	}
	else
	{
		//��������� ������� � ������
		exe_close.SetItemData(exe_close.AddString(ItemSelected), (DWORD)pi.hProcess);
		//��������� �����
		CloseHandle((HANDLE)pi.hThread);
	}

	
}


void COpenExeDlg::OnBnClickedTerminate()
{
	// TODO: �������� ���� ��� ����������� �����������

	int i_selected_item = exe_close.GetCurSel();	// ���������� ������ ���������� ��������
	CString ItemSelected;
	
	DWORD procinfo = exe_close.GetItemData(i_selected_item);
	BOOL cod = TerminateProcess((HANDLE)procinfo, 0);

	if (!cod)
	{
		MessageBox(L"�� ������� ������� �������", L"������", MB_OK);
	}
	
	
	//CDialogEx::OnTimer(nIDEvent);

}


void COpenExeDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: �������� ���� ��� ����������� ��������� ��� ����� ������������
	int l = 0;
	DWORD cod = 0;
	l = exe_close.GetCount();//������ ��������
	for (int i = 0; i < l; i++)
	{
		GetExitCodeProcess((HANDLE)exe_close.GetItemData(i), &cod);
		if (cod != STILL_ACTIVE)
		{
			CloseHandle((HANDLE)exe_close.GetItemData(i));
			exe_close.DeleteString(i);
		}
	}

	CDialogEx::OnTimer(nIDEvent);
}
