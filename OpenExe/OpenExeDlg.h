
// OpenExeDlg.h : ���� ���������
//

#pragma once
#include "afxwin.h"


// ���������� ���� COpenExeDlg
class COpenExeDlg : public CDialogEx
{
// ��������
public:
	COpenExeDlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_OPENEXE_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CString way;
	CListBox exe_open;
	CListBox exe_close;
	afx_msg void OnChangeWay();
	afx_msg void OnBnClickedSearch();
	afx_msg void OnBnClickedStart();
	afx_msg void OnBnClickedTerminate();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
